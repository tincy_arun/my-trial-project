import { Component, OnInit } from '@angular/core';
import { AboutService } from './about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
   title:string ="This is a sample custom directive;";
   months = ["January", "Feburary", "March", "April", "May",
            "June", "July", "August", "September",
            "October", "November", "December"];
   isavailable = false;   //variable is set to true
   istemplate =true;
  constructor(private aboutService: AboutService) { }
  someData = [];
  ngOnInit() {
	  console.log(this.aboutService.cars);
	  this.someData = this.aboutService.cars;
  }

}
