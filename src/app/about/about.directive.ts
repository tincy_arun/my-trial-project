import { Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appAbout]'
})
export class AboutDirective {

  constructor(Element: ElementRef) {
      Element.nativeElement.innerText="Text is changed by changeText Directive. ";
  }

}
