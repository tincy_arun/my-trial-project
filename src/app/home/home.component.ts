import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title:string = 'Home Works';
  appLink:string='Way to go FAQ';
  appTitle: string = 'Click here to know about us';
  appStatus: boolean = true;
  name: string = 'john doe';
  isPipe = true;
  clicked= function () {
        console.log("checking the button click");
        this.router.navigate(['about']);
  };

  todaydate = new Date();
  jsonval = {name:'Rox', age:'25', address:{a1:'Mumbai', a2:'Karnataka'}};

  constructor(private router: Router) { }

  ngOnInit() {

  }

}
